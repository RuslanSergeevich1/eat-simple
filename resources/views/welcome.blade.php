<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ 'laravel - site' }}</title>
        <link href="/css/app.css" rel="stylesheet">
        <!-- Styles -->
    </head>
    <body>
    <header class="header">
        <div class="hamburger hamburger--elastic clickable" tabindex="0"
             aria-label="Menu" role="button" aria-controls="navigation">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <nav>
            <ul class="nav">
                <li><a href="#">About</a></li>
                <li><a href="#">Portfolio</a>
                    <ul>
                        <li><a href="#">item</a></li>
                        <li><a href="#">item</a></li>
                        <li><a href="#">item</a></li>
                        <li><a href="#">item</a></li>
                    </ul>
                </li>
                <li><a href="#">Resume</a>
                    <ul>
                        <li><a href="#">item a lonng submenu</a></li>
                        <li><a href="#">item</a>
                            <ul>
                                <li><a href="#">Ray</a></li>
                                <li><a href="#">Veronica</a></li>
                                <li><a href="#">Bushy</a></li>
                                <li><a href="#">Havoc</a></li>
                            </ul>
                        </li>
                        <li><a href="#">item</a></li>
                        <li><a href="#">item</a></li>
                    </ul>
                </li>
                <li><a href="#">Download</a></li>
                <li><a href="#">Rants</a>
                    <ul>
                        <li><a href="#">item</a></li>
                        <li><a href="#">item</a></li>
                        <li><a href="#">item</a></li>
                        <li><a href="#">item</a></li>
                    </ul>
                </li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>
        <div class="header__social">
            <a href="tel:+79780863686">+7 (978) 0863686</a>
            <a href="https://www.instagram.com/prosto_esh_simf/" target="_blank">
                <i class="fa fa-instagram"> INSTAGRAM</i>
            </a>
        </div>
        <div class="header__content">
            <div class="header__content-logo">
                <a href="/">
                    <img class="header__content-logo-image" src="/images/logo.jpg" alt="logo">
                </a>
            </div>
            <div class="header__content-text">
                <div class="header__content-text-first">Правильное питание</div>
                <div class="header__content-text-second">просто ешь и худей</div>
            </div>
        </div>
    </header>
        <div class="content__fullwidth">

            <div class="content__fullwidth-text">
                <h1>Доставка правильного питания «Просто ешь и худей» в Симферополе!</h1>
                <p>Мы думаем о каждом <a href="/">своём клиенте</a> и перед тем, как открывать доставку здорового питания “Просто Ешь и Худей” мы позаботились о том, чтобы наши блюда были высокого качества, доставляли Вам радость и делали Ваш организм здоровее.</p>
                <p>Основные рационы нашей доставки разбиты на 3 основных и 3 дополнительных приема еды. Каждый прием еды рассчитан так, чтобы Вы не только получали все важнейшие для организма микроэлементы, но и не превышали дневную норму калорий и КБЖУ.</p>
                <p>Мы заботимся о наших клиентах и каждый день доставляем свежеприготовленные рационы. Доставка осуществляется ежедневно с воскресенья по пятницу. В пятницу мы доставляем рационы сразу на 2 дня — субботу и воскресенье. Доставка по Симферополю бесплатная и осуществляется в вечернее время с 18.00 до 21.00</p>
            </div>

            <div class="content__fullwidth-get-order">
                <div class="content__fullwidth-get-order-text">
                    <div class="content__fullwidth-get-order-text_big">
                        Прием заказов до 12.00<br/>
                        Доставка с 18.00 до 21.00
                    </div>
                    <div class="content__fullwidth-get-order-text_small">
                        Менеджер готов ответить на вопросы с 9.00 до 19.00 по тел.: <br/>+7 (978) 0863686
                    </div>
                </div>
            </div>

            <div class="content__sample">

                <div class="content__sample-title">Примеры рационов на день</div>

                <div class="content__sample-grid">

                    <div class="content__sample-item">
                        <div class="content__sample-item-image" style="background-image: url('/images/logo.jpg')">
                            <div class="content__sample-item-image-text">
                                Меню на день
                            </div>
                        </div>
                        <div class="content__sample-item-text">
                            <ul>
                                <li>Каша мультизлаковая на обезжиренном молоке с клубникой и шоколадом</li>
                                <li>Маффины творожные с изюмом и грушей</li>
                                <li>Суп с курицей и грибами</li>
                                <li>Люля-кебаб из индейки с овощами</li>
                                <li>Салат греческий с сыром фета</li>
                                <li>Куриная грудка гриль со свежими овощами</li>
                            </ul>
                            <p>Куриная грудка гриль со свежими овощами</p>
                        </div>
                    </div>

                    <div class="content__sample-item">
                        <div class="content__sample-item-image" style="background-image: url('/images/logo.jpg')">
                            <div class="content__sample-item-image-text">
                                Меню на день
                            </div>
                        </div>
                        <div class="content__sample-item-text">
                            <ul>
                                <li>Каша мультизлаковая на обезжиренном молоке с клубникой и шоколадом</li>
                                <li>Маффины творожные с изюмом и грушей</li>
                                <li>Суп с курицей и грибами</li>
                                <li>Люля-кебаб из индейки с овощами</li>
                                <li>Салат греческий с сыром фета</li>
                                <li>Куриная грудка гриль со свежими овощами</li>
                            </ul>
                            <p>Куриная грудка гриль со свежими овощами</p>
                        </div>
                    </div>

                    <div class="content__sample-item">
                        <div class="content__sample-item-image" style="background-image: url('/images/logo.jpg')">
                            <div class="content__sample-item-image-text">
                                Меню на день
                            </div>
                        </div>
                        <div class="content__sample-item-text">
                            <ul>
                                <li>Каша <a href="/">мультизлаковая</a> на обезжиренном молоке с клубникой и шоколадом</li>
                                <li>Маффины творожные с изюмом и грушей</li>
                                <li>Суп с курицей и грибами</li>
                                <li>Люля-кебаб из индейки с овощами</li>
                                <li>Салат греческий с сыром фета</li>
                                <li>Куриная грудка гриль со свежими овощами</li>
                            </ul>
                            <p>Куриная грудка гриль со свежими овощами</p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="content__fullwidth-order">
                <div class="content__fullwidth-order-text">Закажите свой рацион и начните питаться правильно!</div>
                <div class="content__fullwidth-order-phone">
                    <a class="content__fullwidth-order-phone-link" href="tel:+79780863686">+79780863686</a>
                </div>
            </div>

            <div class="content__sample">
                <div class="content__sample-title">Плюсы заказа готовых рационов питания</div>
                <div class="content__sample-grid">

                    <div class="content__sample-item">
                        <div class="content__sample-item-icon">
                            <i class="fa fa-4x fa-thumbs-up"></i>
                        </div>
                        <div class="content__sample-item-text">
                            <p>Каждый день на вашем столе будет новое вкусное блюдо! Вам не придётся мыть грязную посуду и кухню, после приготовления еды!</p>
                        </div>
                    </div>

                    <div class="content__sample-item">
                        <div class="content__sample-item-icon">
                            <i class="fa fa-4x fa-cutlery"></i>
                        </div>
                        <div class="content__sample-item-text">
                            <p>В Вашем рационе будет разнообразное и полноценное питание: теперь не нужно продумывать меню, считать калории и КБЖУ —  мы уже сделали это за Вас!</p>
                        </div>
                    </div>

                    <div class="content__sample-item">
                        <div class="content__sample-item-icon">
                            <i class="fa fa-4x fa-clock-o"></i>
                        </div>
                        <div class="content__sample-item-text">
                            <p>Мы освободим Ваше время и поможем чувствовать себя лучше! Мы сэкономим Ваши деньги — Вы не будете покупать лишних и вредных продуктов.</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <footer class="footer">
            <div class="footer__text-left">Правильное питание</div>
            <div class="footer__text-right">Правильное питание</div>
            <div class="footer__copyright"><a href="/">Просто Ешь</a> &copy; - {{ date('Y') }}</div>
        </footer>
    </body>
    <!-- Return to Top -->
    <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>
    <script src="{{asset('js/app.js')}}"></script>
</html>
