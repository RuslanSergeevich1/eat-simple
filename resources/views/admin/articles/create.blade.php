@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Создание Статьи</h1>
@stop

@section('content')
    {!! Form::open(['url' => 'admin/articles', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
    {!! Form::label('Title') !!}
    {!! Form::text('title', old('title'), array('required', 'class'=>'form-control', 'placeholder'=>'Title cтатьи')) !!}<br/>
    {!! Form::label('Keywords') !!}
    {!! Form::text('keywords', old('keywords'), array('class'=>'form-control', 'placeholder'=>'Keywords cтатьи')) !!}<br/>
    {!! Form::label('Description') !!}
    {!! Form::text('description', old('description'), array('required', 'class'=>'form-control', 'placeholder'=>'Description cтатьи')) !!}<br/>
    {!! Form::label('URL cтатьи') !!}
    {!! Form::text('url', old('url'), array('required', 'class'=>'form-control translit', 'placeholder'=>'URL cтатьи')) !!}<br/>
    {!! Form::label('Изображение') !!}
    {!! Form::file('img', old('img')) !!}<br/>
    {!! Form::label('Название cтатьи') !!}
    {!! Form::text('name', old('name'), array('required', 'class'=>'form-control', 'placeholder'=>'Название cтатьи')) !!}<br/>
    {!! Form::label('Текст cтатьи') !!}
    {!! Form::textarea('text', old('text'), array('required', 'class'=>'form-control', 'placeholder'=>'Текст cтатьи')) !!}<br/>
    {!! Form::label('Краткое описание cтатьи') !!}
    {!! Form::textarea('small_text', old('small_text'), array('class'=>'form-control', 'placeholder'=>'Краткое описание cтатьи')) !!}<br/>
    {!! Form::label('Публикация статьи') !!}<br/>
    {!! Form::hidden('published', 0) !!}
    Опубликовано: {!! Form::checkbox('published', 1, array('class'=>'form-control')) !!}<br/><br/>
    {!! Form::submit('Добавить статью', array('class'=>'btn btn-primary')) !!}<br/>
    {!! Form::close() !!}
@stop
@push('scripts')
    <script>
        // CKEDITOR.replace( 'text' );
        $(function(){
            $('.translit').liTranslit();
        })
    </script>
@endpush
