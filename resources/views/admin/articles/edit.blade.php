@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Редактирование Статьи</h1>
@stop

@section('content')
    {!! Form::open(['method' => 'PATCH', 'enctype' => 'multipart/form-data', 'files' => true, 'action' => ['Admin\ArticlesController@update', $article->id]]) !!}
    {!! Form::label('Title') !!}
    {!! Form::text('title', $article->title, array('required', 'class'=>'form-control', 'placeholder'=>'Title cтатьи')) !!}<br/>
    {!! Form::label('Keywords') !!}
    {!! Form::text('keywords', $article->keywords, array('class'=>'form-control', 'placeholder'=>'Keywords cтатьи')) !!}<br/>
    {!! Form::label('Description') !!}
    {!! Form::text('description', $article->description, array('required', 'class'=>'form-control', 'placeholder'=>'Description cтатьи')) !!}<br/>
    {!! Form::label('URL cтатьи') !!}
    {!! Form::text('url', $article->url, array('required', 'class'=>'form-control translit', 'placeholder'=>'URL cтатьи')) !!}<br/>
    {!! Form::label('Изображение') !!}<br/>
    @if(!empty($article->img))<img src="/images/articles/thumb_{!! $article->img !!}" class="img-thumbnail"><br/><br/>@endif
    {!! Form::file('img', null) !!}<br/>
    {!! Form::label('Название cтатьи') !!}
    {!! Form::text('name', $article->name, array('required', 'class'=>'form-control', 'placeholder'=>'Название cтатьи')) !!}<br/>
    {!! Form::label('Текст cтатьи') !!}
    {!! Form::textarea('text', $article->text, array('required', 'class'=>'form-control', 'placeholder'=>'Текст cтатьи')) !!}<br/>
    {!! Form::label('Краткое описание cтатьи') !!}
    {!! Form::textarea('small_text', $article->small_text, array( 'class'=>'form-control', 'placeholder'=>'Краткое описание cтатьи')) !!}<br/>
    {!! Form::label('Публикация статьи') !!}<br/>
    Опубликовано:
    {!! Form::hidden('published', 0) !!}
    {!! Form::checkbox('published', 1, $article->published) !!}<br/><br/>
    {!! Form::hidden('updated_at', Carbon\Carbon::now(), array('required', 'class'=>'form-control')) !!}<br/>
    {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}<br/>
    {!! Form::close() !!}
@stop
@push('scripts')
    <script>
        // CKEDITOR.replace( 'text' );
        $(function(){
            $('.translit').liTranslit();
        })
    </script>
@endpush
