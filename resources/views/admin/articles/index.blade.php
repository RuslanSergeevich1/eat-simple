@extends('adminlte::page')

@section('title', 'Список cтатей')

@section('content_header')
    <h1>Список cтатей</h1>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список cтатей</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>URL</th>
                    <th>Дата изменения</th>
                    <th>Публикация</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($articles as $article)
                    <tr>
                        <td>{{$article->id}}</td>
                        <td>{{$article->name}}</td>
                        <td>{{$article->url}}
                        </td>
                        <td>{{($article->updated_at != null) ?
                         $article->updated_at->format('m/d/Y H:i:s') :
                         $article->created_at->format('m/d/Y H:i:s')}}
                        </td>
                        <td>@if ($article->published == 1)<span class="text-success">Да</span> @else <span class="text-danger">Нет</span> @endif </td>
                        <td class="button-width">
                            <a href="/admin/articles/{{$article->id}}/edit">
                                <button type="button" class="btn btn-primary">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </a>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#{{$article->id}}">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="modal fade" id="{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Удалить статью?</h4>
                                        </div>
                                        <div class="modal-body">
                                            После удаления восстановить будет невозможно! Продолжаем?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Закрыть</button>
                                            {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\ArticlesController@destroy', $article->id]]) !!}
                                            {!! Form::submit('Удалить', array('class'=>'btn btn-danger')) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>URL</th>
                    <th>Дата изменения</th>
                    <th>Публикация</th>
                    <th>Действие</th>
                </tr>
                </tfoot>
            </table><br/>
            <div class="col-12">
                <a href="/admin/articles/create"><button type="button" class="btn btn-primary">Добавить статью</button></a>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop



