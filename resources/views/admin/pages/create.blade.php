@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Настройки</h1>
@stop

@section('content')
    {!! Form::open(['url' => 'admin/pages']) !!}
    {!! Form::label('Тайтл') !!}
    {!! Form::text('title', old('title'), array('required', 'class'=>'form-control', 'placeholder'=>'Тайтл страницы')) !!}<br/>
    {!! Form::label('Description') !!}
    {!! Form::text('description', old('description'), array('required', 'class'=>'form-control', 'placeholder'=>'Description страницы')) !!}<br/>
    {!! Form::label('Keywords') !!}
    {!! Form::text('keywords', old('keywords'), array('class'=>'form-control', 'placeholder'=>'Keywords страницы')) !!}<br/>
    {!! Form::label('URL страницы') !!}
    {!! Form::text('url', old('url'), array('required', 'class'=>'form-control translit', 'placeholder'=>'URL страницы')) !!}<br/>
    {!! Form::label('Название страницы') !!}
    {!! Form::text('name', old('name'), array('required', 'class'=>'form-control', 'placeholder'=>'Название страницы')) !!}<br/>
    {!! Form::label('Текст страницы') !!}
    {!! Form::textarea('text', old('text'), array('required', 'class'=>'form-control', 'placeholder'=>'Текст страницы')) !!}<br/>
    {!! Form::label('Публикация страницы') !!}<br/>
    {!! Form::hidden('published', 0) !!}
    Опубликовано: {!! Form::checkbox('published', 1, array('class'=>'form-control')) !!}<br/><br/>
    {!! Form::submit('Добавить страницу', array('class'=>'btn btn-primary')) !!}<br/>
    {!! Form::close() !!}
@stop
@push('scripts')
    <script>
        CKEDITOR.replace( 'text' );
        $(function(){
            $('.translit').liTranslit();
        })
    </script>
@endpush
