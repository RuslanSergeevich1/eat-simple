@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Список страниц</h1>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Список страниц</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>URL</th>
                    <th>Дата изменения</th>
                    <th>Публикация</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                    <tr>
                        <td>{{$page->id}}</td>
                        <td>{{$page->name}}</td>
                        <td>{!! ($page->url == '/') ?
                            '<p class="text-success"><b>Главная страница</b></p>' :
                            $page->url !!}
                        </td>
                        <td>{{($page->updated_at != null) ?
                         $page->updated_at->format('m/d/Y H:i:s') :
                         $page->created_at->format('m/d/Y H:i:s')}}
                        </td>
                        <td>@if ($page->published == 1)<span class="text-success">Да</span> @else <span class="text-danger">Нет</span> @endif </td>
                        <td class="button-width">
                            <a href="/admin/pages/{{$page->id}}/edit">
                                <button type="button" class="btn btn-primary">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </a>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#{{$page->id}}">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="modal fade" id="{{$page->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Удалить страницу?</h4>
                                        </div>
                                        <div class="modal-body">
                                            После удаления восстановить будет невозможно! Продолжаем?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Закрыть</button>
                                            {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\PagesController@destroy', $page->id]]) !!}
                                            {!! Form::submit('Удалить', array('class'=>'btn btn-danger')) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>URL</th>
                    <th>Дата изменения</th>
                    <th>Публикация</th>
                    <th>Действие</th>
                </tr>
                </tfoot>
            </table><br/>
            <div class="col-12">
                <a href="/admin/pages/create"><button type="button" class="btn btn-primary">Добавить страницу</button></a>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@stop



