@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Редактирование Страницы</h1>
@stop

@section('content')
    {!! Form::model($page, ['method' => 'PATCH', 'action' => ['Admin\PagesController@update', $page->id]]) !!}
    {!! Form::label('Тайтл') !!}
    {!! Form::text('title', old('title'), array('required', 'class'=>'form-control', 'placeholder'=>'Тайтл страницы')) !!}<br/>
    {!! Form::label('Description') !!}
    {!! Form::text('description', old('Description'), array('required', 'class'=>'form-control', 'placeholder'=>'Description страницы')) !!}<br/>
    {!! Form::label('Keywords') !!}
    {!! Form::text('keywords', old('Keywords'), array('class'=>'form-control', 'placeholder'=>'Keywords страницы')) !!}<br/>
    {!! Form::label('URL страницы') !!}
    {!! Form::text('url', old('url'), array('required', 'class'=>'form-control translit', 'placeholder'=>'URL страницы')) !!}<br/>
    {!! Form::label('Название страницы') !!}
    {!! Form::text('name', old('name'), array('required', 'class'=>'form-control', 'placeholder'=>'Название страницы')) !!}<br/>
    {!! Form::label('Текст страницы') !!}
    {!! Form::textarea('text', old('text'), array('required', 'class'=>'form-control', 'placeholder'=>'Текст страницы')) !!}<br/>
    {!! Form::label('Публикация страницы') !!}<br/>
    {!! Form::hidden('published', 0) !!}
    Опубликовано: {!! Form::checkbox('published', 1, old('published')) !!}<br/><br/>
    {!! Form::submit('Сохранить', array('class'=>'btn btn-primary')) !!}<br/>
    {!! Form::close() !!}
@stop
@push('scripts')
    <script>
        CKEDITOR.replace( 'text' );
        $(function(){
            $('.translit').liTranslit();
        })
    </script>
@endpush
