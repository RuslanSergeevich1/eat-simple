@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Настройки</h1>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    {!! Form::open(['method' => 'POST', 'enctype' => 'multipart/form-data', 'files' => true, 'action' => 'Admin\SettingsController@index']) !!}
        {!! Form::label('Логотип') !!}<br/>
        @if(!empty($settings->logo))
            <img src="/images/{!! $settings->logo !!}" style="width: 150px;" class="img-thumbnail"><br/><br/>
        @endif
        {!! Form::file('logo', null) !!}<br/>
        {!! Form::label('Телефоны') !!}<br/>
        {!! Form::text('phone1', $settings->phone1, array('class'=>'form-control', 'placeholder'=>'Номер телефона')) !!}<br/>
        {!! Form::text('phone2', $settings->phone2, array('class'=>'form-control', 'placeholder'=>'Номер телефона')) !!}<br/>
        {!! Form::text('phone3', $settings->phone3, array('class'=>'form-control', 'placeholder'=>'Номер телефона')) !!}<br/>
        {!! Form::label('E-mail адреса') !!}<br/>
        {!! Form::text('email', $settings->email, array('class'=>'form-control', 'placeholder'=>'E-mail')) !!}<br/>
        {!! Form::text('email2', $settings->email2, array('class'=>'form-control', 'placeholder'=>'E-mail')) !!}<br/>
        {!! Form::label('Copyright') !!}
        {!! Form::text('copyright', $settings->copyright, array('class'=>'form-control', 'placeholder'=>'copyright')) !!}<br/>
        {!! Form::label('Адрес') !!}<br/>
        {!! Form::textarea('address', $settings->address, array('class'=>'form-control')) !!}<br/>
        {!! Form::label('Метрика и другие скрипты') !!}<br/>
        {!! Form::textarea('metrika', $settings->metrika, array('class'=>'form-control')) !!}<br/>
        {!! Form::submit('Сохранить', array('class'=>'btn btn-primary submit_settings')) !!}
    {!! Form::close() !!}
@stop



