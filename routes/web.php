<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::resource('/admin/pages', 'Admin\PagesController');
    Route::resource('/admin/articles', 'Admin\ArticlesController');
    Route::get('/admin/settings', 'Admin\SettingsController@index')->name('settings');
    Route::post('/admin/settings', 'Admin\SettingsController@update')->name('settings');
});
