<?php

namespace App\Http\Controllers\Admin;

use App\Settings;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = Settings::findOrFail(1);

        return view('admin/settings/index', compact('settings'));
    }


    public function update(Request $request)
    {

        $data = $request->all();
        $settings = Settings::findOrFail(1);
        $settings->fill($data)->save();

        if ($request->hasFile('logo')){
            $file = $request->file('logo');
            $name =  'logo.' .$file->getClientOriginalExtension();
            File::delete( public_path() . '/images/' . $settings->logo);
            $file->move(public_path().'/images/', $name);
            $settings->logo = $name;
            $settings->update();
        }

        return redirect('admin/settings')->with('status', 'Настройки сохранены!');

    }
}
