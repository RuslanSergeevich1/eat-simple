<?php

namespace App\Http\Controllers\Admin;

use App\Articles;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;

class ArticlesController extends Controller
{

    public function index()
    {
        $articles = Articles::all();

        return view('admin/articles/index', compact('articles'));
    }

    public function create()
    {
        return view('admin/articles/create');
    }

    public function store(Request $request)
    {

        $data = $request->all();
        $data['published_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        if ($request->hasFile('img')){
            $file = $request->file('img');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $data['img'] = $timestamp. '-' .$file->getClientOriginalName();
            $file->move(public_path().'/images/articles/', $data['img']);
            $path = public_path().'/images/articles/'.'thumb_'.$data['img'];
            $imagePath = public_path() . '/images/articles/' . $data['img'];
            $img = Image::make($imagePath);
            $img->resize(null, 175, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($path);
            $data['img'] = $timestamp. '-' .$file->getClientOriginalName();
        }else{
            $data['img']='';
        }

        Articles::create($data);

        return redirect('admin/articles')->with('status', 'Статья создана!');
    }

    public function edit($id)
    {
        $article = Articles::findOrFail($id);

        return view('admin/articles/edit', compact('article'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $article = Articles::findOrFail($id);
        $article->fill($data)->save();

        if ($request->hasFile('img')){
            $file = $request->file('img');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-' .$file->getClientOriginalName();
            $file->move(public_path().'/images/articles/', $name);
            $path = public_path().'/images/articles/'.'thumb_'.$name;
            $imagePath = public_path() . '/images/articles/' . $name;
            $article->img = $name;
            $img = Image::make($imagePath);
            $img->resize(null, 175, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($path);
            $article->update();
        }

        return redirect('admin/articles')->with('status', 'Статья сохранена!');
    }

    public function destroy($id)
    {
        $article = Articles::findOrFail($id);
        $files = array(
            public_path().'/images/articles/'.'thumb_'.$article->img,
            public_path() . '/images/articles/' . $article->img
        );
        File::delete($files);
        $article->delete();

        return redirect('admin/articles')->with('status', 'Статья удалена!');
    }

}
