<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pages;
use Carbon\Carbon;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Pages::all();

        return view('admin/pages/index', compact('pages'));
    }

    public function create()
    {
        return view('admin/pages/create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['published_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        Pages::create($data);

        return redirect('admin/pages')->with('status', 'Страница создана!');
    }

    public function edit($id)
    {
        $page = Pages::findOrFail($id);

        return view('admin/pages/edit', compact('page'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $page = Pages::findOrFail($id);
        $page->fill($data)->save();

        return redirect('admin/pages')->with('status', 'Страница сохранена!');
    }

    public function destroy($id)
    {
        $pages = Pages::findOrFail($id);
        $pages->delete();

        return redirect('admin/pages')->with('status', 'Страница удалена!');
    }
}
