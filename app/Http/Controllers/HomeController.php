<?php

namespace App\Http\Controllers;
use App\Pages;
use App\Settings;


class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $settings = Settings::where('id', 1)->first();
        $page = Pages::where('url', '/')->first();
        return view('home', compact('settings', 'page'));
    }
}
