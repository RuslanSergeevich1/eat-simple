<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'id' => 1,
            'phone1' => '123456789',
            'phone2' => '123456789',
            'phone3' => '123456789',
            'email' => Str::random(10).'@gmail.com',
            'email2' => Str::random(10).'@gmail.com',
            'vk' => 'https://vk.com',
            'fb' => 'https://www.facebook.com',
            'inst' => 'https://instagram.com',
            'copyright' => 'copyright',
            'metrika' => 'metrika',
            'address' => 'address',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
